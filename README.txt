
socialSharePrivacy
==================
Author: Wolfgang Reszel <http://drupal.org/user/80975>
For:    http://www.werbeagentur-willers.de


Description
===========
This module is a quick implementation of the socialSharePrivacy script by Heise Zeitschriften Verlag.
http://www.heise.de/extras/socialshareprivacy/

With socialSharePrivacy you can add Facebook share, Twitter share and Google+ share buttons to your web site or blog which respects the privacy of your visitors. The buttons are implementet as static buttons which don't force a connection to the social networks, so they are not able to track the visitors IP. When you press the static buttons, this module will replace them with the original share code for the social networks to retain the full functionality. Users are able to always enable the non-static social network buttons if they like.


Requirements
============
Drupal 7.x


Support
=======
If you find any Bugs use the issue queue on drupal.org:
http://drupal.org/project/issues/1288046

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License
=======
This module and its containing code from Heise Zeitschriften Verlag is licensed under the MIT License. See LICENSE.txt for more information.
