<?php
/**
 * @file
 * Administration settings form area for socialSharePrivacy module
 */

/**
 * Form for settings for the socialSharePrivacy module
 */
function socialshareprivacy_admin_settings($form, &$form_state) {
	$form['socialshareprivacy_types'] = array(
		'#type' => 'checkboxes',
		'#title' => t('Content types'),
		'#description' => t('Which content types to apply the socialSharePrivacy buttons to. There’s also a special <a href="/admin/structure/block/manage/socialshareprivacy/socialSharePrivacy/configure">socialSharePrivacy block</a> to add social links to views or other pages.'),
		'#options' => node_type_get_names(),
		'#default_value' => variable_get('socialshareprivacy_types', array()),
	);

	$form['socialshareprivacy_viewmode'] = array(
		'#type' => 'checkboxes',
		'#title' => t('View mode'),
		'#description' => t('Where to show the socialSharePrivacy buttons.'),
		'#options' => array(
			'content' => t('Full view'),
			'summary' => t('Summary (Teaser)'),
		),
		'#default_value' => variable_get('socialshareprivacy_viewmode', array()),
	);

  $form['socialshareprivacy_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Weight'),
    '#description' => t('The weight of which the socialSharePrivacy widget should appear on the content.'),
    '#default_value' => variable_get('socialshareprivacy_weight', '100'),
    '#size' => 5,
  );

	$form['socialshareprivacy_settings_position'] = array(
		'#type' => 'select',
		'#title' => t('Position of settings menu'),
		'#description' => t('Expand the flyout menu to the bottom or the top.'),
		'#options' => array(
			'top' => t('Top'),
			'bottom' => t('Bottom'),
		),
		'#default_value' => variable_get('socialshareprivacy_settings_position', 'bottom'),
	);

  $form['facebook'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook'),
  );

	$form['facebook']['socialshareprivacy_facebook_enabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Use Facebook'),
		'#default_value' => variable_get('socialshareprivacy_facebook_enabled', true),
	);

  $form['facebook']['socialshareprivacy_facebook_referrertrack'] = array(
    '#type' => 'textfield',
    '#title' => t('Tracking parameter'),
    '#description' => t('The tracking parameter will be added to the end of the URL which is useful for tracking the referrer.'),
    '#default_value' => variable_get('socialshareprivacy_facebook_referrertrack', ''),
  );

  $form['facebook']['socialshareprivacy_facebook_action'] = array(
    '#type' => 'radios',
    '#title' => t('Button label'),
    '#options' => array(
      'recommend' => t('Recommend'),
      'like' => t('Like'),
    ),
    '#default_value' => variable_get('socialshareprivacy_facebook_action', 'recommend'),
  );

  $form['twitter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Twitter'),
  );

	$form['twitter']['socialshareprivacy_twitter_enabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Use Twitter'),
		'#default_value' => variable_get('socialshareprivacy_twitter_enabled', true),
	);

  $form['twitter']['socialshareprivacy_twitter_referrertrack'] = array(
    '#type' => 'textfield',
    '#title' => t('Tracking parameter'),
    '#description' => t('The tracking parameter will be added to the end of the URL which is useful for tracking the referrer.'),
    '#default_value' => variable_get('socialshareprivacy_twitter_referrertrack', ''),
  );

  $form['googleplus'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google+'),
  );

	$form['googleplus']['socialshareprivacy_googleplus_enabled'] = array(
		'#type' => 'checkbox',
		'#title' => t('Use Google+'),
		'#default_value' => variable_get('socialshareprivacy_googleplus_enabled', true),
	);

  $form['googleplus']['socialshareprivacy_googleplus_referrertrack'] = array(
    '#type' => 'textfield',
    '#title' => t('Tracking parameter'),
    '#description' => t('The tracking parameter will be added to the end of the URL which is useful for tracking the referrer.'),
    '#default_value' => variable_get('socialshareprivacy_googleplus_referrertrack', ''),
  );

  $form['socialshareprivacy_infolink'] = array(
    '#type' => 'textfield',
    '#title' => t('Info link'),
    '#description' => t('Link to detailed privacy information.'),
    '#default_value' => variable_get('socialshareprivacy_infolink', 'http://www.heise.de/ct/artikel/2-Klicks-fuer-mehr-Datenschutz-1333879.html'),
  );

  $form['socialshareprivacy_cookiepath'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie path'),
    '#description' => t('Valid path of the Cookie.'),
    '#default_value' => variable_get('socialshareprivacy_cookiepath', '/'),
  );

  $form['socialshareprivacy_cookiedomain'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie domain'),
    '#description' => t('Domain the Cookie is valid for.'),
    '#default_value' => variable_get('socialshareprivacy_cookiedomain', 'document.location.host'),
  );

  $form['socialshareprivacy_cookieexpires'] = array(
    '#type' => 'textfield',
    '#title' => t('Cookie expires'),
    '#description' => t('Expiration time of the Cookie.'),
    '#default_value' => variable_get('socialshareprivacy_cookieexpires', '365'),
  );

	$form['opengraph'] = array(
    '#type' => 'fieldset',
    '#title' => t('Open Graph'),
  );

	$form['opengraph']['socialshareprivacy_opengraph'] = array(
		'#type' => 'select',
		'#title' => t('Add Open Graph meta tags'),
		'#description' => t('Add the Open Graph meta tags OG:TITLE and OG:DESCRIPTION using node title and summary.'),
		'#options' => array(
			'off' => t('Never'),
			'contenttypes' => t('Only on the content types where socialSharePrivacy is visible'),
			'all' => t('Always'),
		),
		'#default_value' => variable_get('socialshareprivacy_opengraph', 'contenttypes'),
	);

	$form['opengraph']['socialshareprivacy_image_fiels_markup'] = array(
		'#markup' => '<br /><label>'.t('Select the image fields which should be used for OG:IMAGE. This let you sepcify the images which are choosable when sharing a content URL.').'</label>',
	);

  $form['opengraph']['socialshareprivacy_image_fields'] = array(
    '#tree' => true,
  );
  $imagefields = array();
  $count = 0;
  foreach (field_info_fields() as $key => $value) {
    if ($value['type']=='image') {
      $variable_get = variable_get('socialshareprivacy_image_fields', array($key => array ('status'=>($count == 0 ? true : false), 'weight'=>$count)) );
      $form['opengraph']['socialshareprivacy_image_fields'][$key]['status'] = array(
        '#type' => 'checkbox',
        '#title' => $key,
        '#default_value' => $variable_get[$key]['status'],
      ); 
      $form['opengraph']['socialshareprivacy_image_fields'][$key]['weight'] = array(
        '#type' => 'weight',
        '#delta' => 20,
        '#prefix' => t('Weight').': ',
        '#suffix' => '<br />',
        '#default_value' => $variable_get[$key]['weight'],
      ); 
      $count++;
    }
  }
  
  drupal_add_css(drupal_get_path('module', 'socialshareprivacy') . '/socialshareprivacy.css');

  return system_settings_form($form);
}
