<?php

/**
 * @file
 * Adds social network buttons to content with the socialSharePrivacy technology by Heise Zeitschriften Verlag
 */

/**
 * Implements hook_help()
 */
function socialshareprivacy_help($path, $arg) {
  switch ($path) {
    case 'admin/config/user-interface/socialshareprivacy':
    case 'admin/help#socialshareprivacy':
      return '<p>' . t('Adds social network buttons to content types with the 2-click-method “<a href="http://www.heise.de/extras/socialshareprivacy/" target="_blank">socialSharePrivacy</a>” by Heise Zeitschriften Verlag. So the networks won\'t receive data unless the visitor wants to.') . '</p>';
  }
}

/**
 * Implements hook_permission()
 */
function socialshareprivacy_permission() {
  return array(
    'administer socialshareprivacy' => array(
      'title' => t('administer socialSharePrivacy'),
    ),
    'access socialshareprivacy' => array(
      'title' => t('access socialSharePrivacy'),
    ),
  );
}

/**
 * Implements hook_menu()
 */
function socialshareprivacy_menu() {
  $item['admin/config/user-interface/socialshareprivacy'] = array(
    'title' => 'socialSharePrivacy',
    'description' => 'Provides the configuration options for how socialSharePrivacy operates on the site.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('socialshareprivacy_admin_settings'),
    'access arguments' => array('administer socialshareprivacy'),
    'file' => 'socialshareprivacy.admin.inc',
  );

  return $item;
}

/**
 * Implements hook_init()
 */
function socialshareprivacy_init() {
  #drupal_add_css(drupal_get_path('module', 'socialshareprivacy') . '/socialshareprivacy.css');
  drupal_add_js(drupal_get_path('module', 'socialshareprivacy') . '/jquery.socialshareprivacy.js');
}

function socialshareprivacy_preprocess_node(&$variables) {
  // Make sure we're on the right content type.
  if (!in_array($variables['node']->type, variable_get('socialshareprivacy_types', array()), TRUE)) {
    return NULL;
  }

  // Make sure the user has access to use socialSharePrivacy.
  if (!user_access('access socialshareprivacy')) {
    return NULL;
  }
  $variables['classes_array'][] = 'socialshareprivacy';
}

/**
 * Implements hook_node_view()
 */
function socialshareprivacy_node_view($node, $view_mode = 'full') {
  // Make sure we're on the right content type.
  if (!in_array($node->type, variable_get('socialshareprivacy_types', array()), TRUE)) {
    return NULL;
  }

  // We only render the button on full pages and on teasers
  if (($view_mode != 'teaser') && ($view_mode != 'full')) {
    return NULL;
  }

  // Make sure the user has access to use socialSharePrivacy.
  if (!user_access('access socialshareprivacy')) {
    return NULL;
  }
  // Retrieve the location where we should show it
  $location = variable_get('socialshareprivacy_viewmode', array());

  // Check if the current $view_mode has content to show
  if (($view_mode == 'teaser' && !empty($location['summary'])) || ($view_mode == 'full' && !empty($location['content']))) {

    // Retrieve the weight and url of the node
    $url = url('node/' . $node->nid, array('absolute' => TRUE));
    $weight = check_plain(variable_get('socialshareprivacy_weight', 100));

    // Add and theme the button
    $node->content['socialshareprivacy'] = array(
      '#markup' => theme('socialshareprivacy', array('nid' => $node->nid, 'position' => variable_get('socialshareprivacy_settings_position','bottom'))),
      '#weight' => is_numeric($weight) ? (int)$weight : 100,
    );

    if ($node->language == 'de') {
      $facebook_dummy = "/".drupal_get_path('module', 'socialshareprivacy')."/images/dummy_facebook.png";
    } else {
      $facebook_dummy = "/".drupal_get_path('module', 'socialshareprivacy')."/images/dummy_facebook_en.png";
    }

    $script_code = _socialshareprivacy_get_script_code('.socialshareprivacy-box.nid-'.$node->nid, $node->language, $url, $facebook_dummy);
    
    if ( (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') ) {
      $node->content['socialshareprivacy']['#markup'] = $node->content['socialshareprivacy']['#markup']."<script type=\"text/javascript\">$script_code</script>";
    } else {
      drupal_add_js($script_code,'inline');
    }

  }
}

/**
 * Implements hook_theme()
 */
function socialshareprivacy_theme($existing, $type, $theme, $path) {
  return array(
    'socialshareprivacy' => array(
      'variables' => array(
        'nid' => NULL,
        'position' => NULL,
      ),
    ),
  );
}

/**
 * Themes socialSharePrivacy button box
 */
function theme_socialshareprivacy($variables) {
  $output = '<div class="socialshareprivacy-box nid-'.$variables['nid'].' '.$variables['position'].'">';
  $output .= '</div>';
  return $output;
}

/**
 * Implements hook_block_info();
 */
function socialshareprivacy_block_info() {
  $blocks['socialSharePrivacy'] = array(
    'info'    => t('socialSharePrivacy')
  );
  
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function socialshareprivacy_block_view($block_name = '') {
  global $language;
  if ($block_name == 'socialSharePrivacy') {

    // Make sure the user has access to use socialSharePrivacy.
    if (!user_access('access socialshareprivacy')) {
      return NULL;
    }

    // Retrieve the url of the page
    $url = url(drupal_get_path_alias(), array('absolute' => TRUE));

    if ($language->language == 'de') {
      $facebook_dummy = "/".drupal_get_path('module', 'socialshareprivacy')."/images/dummy_facebook.png";
    } else {
      $facebook_dummy = "/".drupal_get_path('module', 'socialshareprivacy')."/images/dummy_facebook_en.png";
    }

    $script_code = _socialshareprivacy_get_script_code('#socialshareprivacy-block', $language->language, $url, $facebook_dummy);

    $block = array(
      'subject' => '<none>',
      'content' => "<div id=\"socialshareprivacy-block\"></div>",
    );
    
    drupal_add_js($script_code,'inline');
          
    return $block;
  }
}

/**
 * Implements hook_block_configure().
 */
function socialshareprivacy_block_configure($block_name = '') {
  if ($block_name == 'socialSharePrivacy') {
    $form['help'] = array(
      '#markup' => t('Additional options at the <a href="/admin/config/user-interface/socialshareprivacy">configuration page of socialSharePrivacy</a>.'),
    );
    return $form;
  }
}

/**
 * Add Teaser as OG:DESCRIPTION to html head
 */
function socialshareprivacy_preprocess_page($variables) {
  if (!@$variables['node']) return NULL;

  if (variable_get('socialshareprivacy_opengraph', 'contenttypes') == 'off') return NULL;
  
  if (variable_get('socialshareprivacy_opengraph', 'contenttypes') == 'contenttypes') {
    // Make sure we're on the right content type.
    if (!in_array($variables['node']->type, variable_get('socialshareprivacy_types', array()), TRUE)) {
      return NULL;
    }

    // Make sure the user has access to use socialSharePrivacy.
    if (!user_access('access socialshareprivacy')) {
      return NULL;
    }
  }

  $body = field_get_items('node', $variables['node'], 'body');
  $teaser = trim(strip_tags($body[0]['safe_summary']));

  if (empty($teaser) && @$variables['node']->field_teaser) {
    $body = field_get_items('node', $variables['node'], 'field_teaser');
    $teaser = trim(strip_tags($body[0]['safe_value']));
  }

  if (empty($teaser)) {
    $teaser = text_summary(trim((strip_tags($body[0]['safe_value']))));
  }

  $og_description = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:description',
      'content' =>  $teaser
    )
  );
  $og_title = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'property' => 'og:title',
      'content' =>  str_replace(array("   ", "  "), " ", $variables['node']->title)
    )
  );
  drupal_add_html_head( $og_description, 'og_description' );
  drupal_add_html_head( $og_title, 'og_title' );
  
  $imagefields = variable_get('socialshareprivacy_image_fields');
  uasort($imagefields,'drupal_sort_weight');

  $images = array();
  foreach ($imagefields as $key => $value) {
    if(!$value['status']) continue;
    if (property_exists($variables['node'],$key)) {
      $imagefield = field_get_items('node', $variables['node'], $key);
      if ($imagefield) $images = array_merge($images, $imagefield);
    }
  }
  foreach ($images as $key => $image) {
    if (!empty($image['uri'])) {
      $og_image = array(
        '#type' => 'html_tag',
        '#tag' => 'meta',
        '#weight' => $key+1,
        '#attributes' => array(
          'property' => 'og:image',
          'content' =>  ($key == 0 ? image_style_url('medium',$image['uri']) : image_style_url('thumbnail',$image['uri']))
        )
      );
      drupal_add_html_head( $og_image, 'og_image_'.$key );
    }
  }
}

function _socialshareprivacy_get_script_code($classId, $language, $url, $facebook_dummy) {
  $cookie_domain = check_plain(variable_get('socialshareprivacy_cookiedomain', 'document.location.host'));
  if (preg_match('/^window\.|^document\./', $cookie_domain) < 1) $cookie_domain = "'$cookie_domain'";

  return "
  jQuery(document).ready(function($){
    if($('".$classId."').length > 0){
      $('".$classId."').socialSharePrivacy({
        services : {
          facebook : {
            'status'         : '".(variable_get('socialshareprivacy_facebook_enabled', true) ? 'on' : 'off')."',
            'dummy_img'      : '".$facebook_dummy."',
            'txt_info'       : '".addslashes(t("2 clicks for more privacy: Click on this button to activate the Facebook share button. Activating the share button will already transfer data to Facebook. See more on <em>i</em>"))."',
            'language'       : '"._socialshareprivacy_map_facebook_language($language)."',
            'referrer_track' : '".check_plain(variable_get('socialshareprivacy_facebook_referrertrack', ''))."',
            'action'         : '".check_plain(variable_get('socialshareprivacy_facebook_action', 'recommend'))."'
          }, 
          twitter : {
            'status'         : '".(variable_get('socialshareprivacy_twitter_enabled', true) ? 'on' : 'off')."',
            'tweet_text'     : '".trim(nl2br(html_entity_decode(strip_tags(str_ireplace('..','.',$node->title.'. ').$node->content['body']['#items'][0]['safe_summary']),ENT_COMPAT,'UTF-8')))."',
            'dummy_img'      : '/".drupal_get_path('module', 'socialshareprivacy')."/images/dummy_twitter.png',
            'txt_info'       : '".addslashes(t("2 clicks for more privacy: Click on this button to activate the Twitter share button. Activating the share button will already transfer data to Twitter. See more on <em>i</em>"))."',
            'language'       : '".$language."',
            'referrer_track' : '".check_plain(variable_get('socialshareprivacy_twitter_referrertrack', ''))."'              
          },
          gplus : {
            'status'         : '".(variable_get('socialshareprivacy_googleplus_enabled', true) ? 'on' : 'off')."',
            'dummy_img'      : '/".drupal_get_path('module', 'socialshareprivacy')."/images/dummy_gplus.png',
            'txt_info'       : '".addslashes(t("2 clicks for more privacy: Click on this button to activate the Google+ share button. Activating the share button will already transfer data to Google+. See more on <em>i</em>"))."',
            'language'       : '".$language."',
            'referrer_track' : '".check_plain(variable_get('socialshareprivacy_googleplus_referrertrack', ''))."'              
          }
        },
        'css_path'       : '/".drupal_get_path('module', 'socialshareprivacy')."/socialshareprivacy.css',
        'txt_help'       : '".addslashes(t("If you enable these options, Facebook, Twitter or Google will receive and strore information about your visitors. Read more by clicking on <em>i</em>"))."',
        'settings_perma' : '".addslashes(t("Agree for permanent activation and data transfer:"))."',
        'info_link'      : '".check_plain(variable_get('socialshareprivacy_infolink', 'http://www.heise.de/ct/artikel/2-Klicks-fuer-mehr-Datenschutz-1333879.html'))."',
        'cookie_path'    : '".check_plain(variable_get('socialshareprivacy_cookiepath', '/'))."',
        'cookie_domain'  : ".$cookie_domain.",
        'cookie_expires' : '".check_plain(variable_get('socialshareprivacy_cookieexpires', '365'))."',
        'uri'            : '".$url."'
      });
      $('.settings_info_menu a').load().attr('target','_blank'); // open info link in new window 
    }
  });
  ";
}

function _socialshareprivacy_map_facebook_language($language) {
  $lang_map = array(
    'af' => 'af_ZA',
    'ar' => 'ar_AR',
    'ay' => 'ay_BO',
    'az' => 'az_AZ',
    'be' => 'be_BY',
    'bg' => 'bg_BG',
    'bn' => 'bn_IN',
    'bs' => 'bs_BA',
    'ca' => 'ca_ES',
    'ck' => 'ck_US',
    'cs' => 'cs_CZ',
    'cy' => 'cy_GB',
    'da' => 'da_DK',
    'de' => 'de_DE',
    'el' => 'el_GR',
    'en' => 'en_US',
    'eo' => 'eo_EO',
    'es' => 'es_ES',
    'et' => 'et_EE',
    'eu' => 'eu_ES',
    'fa' => 'fa_IR',
    'fb' => 'fb_FI',
    'fb' => 'fb_LT',
    'fi' => 'fi_FI',
    'fo' => 'fo_FO',
    'fr' => 'fr_CA',
    'fr' => 'fr_FR',
    'ga' => 'ga_IE',
    'gl' => 'gl_ES',
    'gn' => 'gn_PY',
    'gu' => 'gu_IN',
    'he' => 'he_IL',
    'hi' => 'hi_IN',
    'hr' => 'hr_HR',
    'hu' => 'hu_HU',
    'hy' => 'hy_AM',
    'id' => 'id_ID',
    'is' => 'is_IS',
    'it' => 'it_IT',
    'ja' => 'ja_JP',
    'jv' => 'jv_ID',
    'ka' => 'ka_GE',
    'kk' => 'kk_KZ',
    'km' => 'km_KH',
    'kn' => 'kn_IN',
    'ko' => 'ko_KR',
    'ku' => 'ku_TR',
    'la' => 'la_VA',
    'li' => 'li_NL',
    'lt' => 'lt_LT',
    'lv' => 'lv_LV',
    'mg' => 'mg_MG',
    'mk' => 'mk_MK',
    'ml' => 'ml_IN',
    'mn' => 'mn_MN',
    'mr' => 'mr_IN',
    'ms' => 'ms_MY',
    'mt' => 'mt_MT',
    'nb' => 'nb_NO',
    'ne' => 'ne_NP',
    'nl' => 'nl_BE',
    'nl' => 'nl_NL',
    'nn' => 'nn_NO',
    'pa' => 'pa_IN',
    'pl' => 'pl_PL',
    'ps' => 'ps_AF',
    'pt' => 'pt_PT',
    'qu' => 'qu_PE',
    'rm' => 'rm_CH',
    'ro' => 'ro_RO',
    'ru' => 'ru_RU',
    'sa' => 'sa_IN',
    'se' => 'se_NO',
    'sk' => 'sk_SK',
    'sl' => 'sl_SI',
    'so' => 'so_SO',
    'sq' => 'sq_AL',
    'sr' => 'sr_RS',
    'sv' => 'sv_SE',
    'sw' => 'sw_KE',
    'sy' => 'sy_SY',
    'ta' => 'ta_IN',
    'te' => 'te_IN',
    'tg' => 'tg_TJ',
    'th' => 'th_TH',
    'tl' => 'tl_PH',
    'tl' => 'tl_ST',
    'tr' => 'tr_TR',
    'tt' => 'tt_RU',
    'uk' => 'uk_UA',
    'ur' => 'ur_PK',
    'uz' => 'uz_UZ',
    'vi' => 'vi_VN',
    'xh' => 'xh_ZA',
    'yi' => 'yi_DE',
    'zh' => 'zh_CN',
    'zu' => 'zu_ZA',
  );
  $return = @$lang_map[$language];
  if (empty($return)) $return = 'en_US';
  return $return;
}